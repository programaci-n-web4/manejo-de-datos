/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author emoru
 */


import java.util.ArrayList;
import java.sql.*;

public class DbProducto extends DbManejador implements DbPersistencia{

    @Override
    public void insertar(Object objecto) throws Exception {
        Productos pro= new Productos();
        pro =(Productos) objecto;
        
        String consulta = "insert into "
        + "productos (codigo,nombre,fecha,precio,status) values(?,?,?,?,?)";
        
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar los valores ala consulta

                this.sqlConsulta.setString(1,pro.getCodigo());
                this.sqlConsulta.setString(2,pro.getNombre());
                this.sqlConsulta.setString(3,pro.getFecha());
                this.sqlConsulta.setFloat(4,pro.getPrecio());
                this.sqlConsulta.setInt(5,pro.getStatus());
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                System.err.println("Surgio un error al insertar"+ e.getMessage());
            }
        }
    }
    @Override
    public void actualizar(Object objeto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;

        String consulta = "UPDATE productos SET nombre = ?, fecha = ?, precio = ?, status = ? WHERE codigo = ?";

        if (this.conectar()) {
            try {
                System.err.println("Se conecto correctamente");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);

                // asignacion de valores a la consulta
                this.sqlConsulta.setString(1, pro.getNombre());
                this.sqlConsulta.setString(2, pro.getFecha());
                this.sqlConsulta.setFloat(3, pro.getPrecio());
                this.sqlConsulta.setInt(4, pro.getStatus());
                this.sqlConsulta.setString(5, pro.getCodigo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();

            } catch (SQLException e) {
                System.err.println("Surgio un error al actualizar el producto" + e.getMessage());
            }
        }
    }
            
    @Override
    public void habilitar(Object object,String codigo) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;
            String consulta ="update productos set status = 1 where codigo = ?";
            if(this.conectar()){
                try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar los valores ala consulta

                this.sqlConsulta.setString(1, codigo);
                
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch(SQLException e){
               System.err.println("Surgio un error al insertar"+ e.getMessage());
            }
        }
    }

    @Override
    public void deshablitar(Object object,String codigo) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;
            String consulta ="update productos set status = 0 where codigo = ?";
            if(this.conectar()){
                try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar los valores ala consulta

                this.sqlConsulta.setString(1, codigo);
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch(SQLException e){
               System.err.println("Surgio un error al insertar"+ e.getMessage());
            }
        }
    }

    @Override
    public boolean isExiste(String codigo) throws Exception {
        String consulta = "SELECT COUNT(*) FROM productos WHERE codigo = ?";
        boolean existe = false;

        if (this.conectar()) {
            try {
                System.out.println("Se conectó.. buscando productos con el código: " + codigo);
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, codigo);
                ResultSet rs = this.sqlConsulta.executeQuery();
                if (rs.next()) {
                    int count = rs.getInt(1);
                    existe = (count > 0);
                }
            } catch (SQLException e) {
                System.err.println("Error al verificar la existencia del producto: " + e.getMessage());
                throw e;
            } finally {
                this.desconectar();
            }
        }
        return existe;
    }

    @Override
    public ArrayList lista() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;
        if(this.conectar()){
            String consulta = "Select * from productos where status = 0 order by codigo";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            this.registro=this.sqlConsulta.executeQuery();
                while(this.registro.next()){
                    pro = new Productos();
                    pro.setIdProducto(this.registro.getInt("idProductos"));
                    pro.setCodigo(this.registro.getString("codigo"));
                    pro.setPrecio(this.registro.getFloat("Precio"));
                    pro.setFecha(this.registro.getString("fecha"));
                    pro.setStatus(this.registro.getInt("status"));
                    pro.setNombre(this.registro.getString("nombre"));
                    lista.add(pro);
                }
        }
        this.desconectar();
        return lista;
    }

    public ArrayList lista2() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;
        if(this.conectar()){
            String consulta = "Select * from productos where status = 1 order by codigo";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            this.registro=this.sqlConsulta.executeQuery();
                while(this.registro.next()){
                    pro = new Productos();
                    pro.setIdProducto(this.registro.getInt("idProductos"));
                    pro.setCodigo(this.registro.getString("codigo"));
                    pro.setPrecio(this.registro.getFloat("Precio"));
                    pro.setFecha(this.registro.getString("fecha"));
                    pro.setStatus(this.registro.getInt("status"));
                    pro.setNombre(this.registro.getString("nombre"));
                    lista.add(pro);
                }
        }
        this.desconectar();
        return lista;
    }
    

    @Override
    public ArrayList lista(String criterio) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object buscardes(String codigo) throws Exception {
        Productos pro = new Productos();
        if(this.conectar()){
            String consulta = "Select * from productos where codigo= ? and status =1";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registro=this.sqlConsulta.executeQuery();
            if(this.registro.next()){
                pro.setIdProducto(this.registro.getInt("idProductos"));
                pro.setPrecio(this.registro.getFloat("Precio"));
                pro.setFecha(this.registro.getString("fecha"));
                pro.setStatus(this.registro.getInt("status"));
                pro.setNombre(this.registro.getString("nombre"));
            }
        }
        this.desconectar();
        return pro;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        if(this.conectar()){
            String consulta = "Select * from productos where codigo =?";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registro=this.sqlConsulta.executeQuery();
            if(this.registro.next()){
                pro.setIdProducto(this.registro.getInt("idProductos"));
                pro.setCodigo(this.registro.getString("codigo"));
                pro.setPrecio(this.registro.getFloat("Precio"));
                pro.setFecha(this.registro.getString("fecha"));
                pro.setStatus(this.registro.getInt("status"));
                pro.setNombre(this.registro.getString("nombre"));
            }
        }
        this.desconectar();
        return pro;
    }
    @Override
    public Object buscar(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}