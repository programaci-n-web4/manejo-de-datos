package modelo;
import java.sql.*;
public abstract class DbManejador {
    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registro;
    private String usuario;
    private String dataBase;
    private String contraseña;
    private String drive;
    private String url;

    public DbManejador(Connection conexion, PreparedStatement sqlConsulta, ResultSet registro, String usuario, String dataBase, String contraseña, String drive, String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registro = registro;
        this.usuario = usuario;
        this.dataBase = dataBase;
        this.contraseña = contraseña;
        this.drive = drive;
        this.url = url ;
        isDriver();
    }

    public DbManejador() {
        this.drive = "com.mysql.jdbc.Driver";
        this.dataBase = "sistemas";
        this.usuario = "root";
        this.contraseña="";
        this.url= "jdbc:mysql://localhost/sistemas";
        isDriver();
    }
    
    public boolean isDriver(){
        boolean exito = false;
        try{
            Class.forName(drive);
            exito = true;
        }catch(ClassNotFoundException e){
            exito = false;
            System.err.println("Surgio un error" + e.getMessage());
            System.exit(-1);
        }
        return exito;
    }
    
    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }

    public ResultSet getRegistro() {
        return registro;
    }

    public void setRegistro(ResultSet registro) {
        this.registro = registro;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDataBase() {
        return dataBase;
    }

    public void setDataBase(String dataBase) {
        this.dataBase = dataBase;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    public boolean isDrive(){
        boolean exito= false;
        try{
            Class.forName(drive);
           exito =true;
        }catch(ClassNotFoundException e){
            exito= false;
            System.err.println("Surgio un error" + e.getMessage());
            System.exit(-1);
        }
        return exito;
    }
    
    public boolean conectar(){
        boolean exito = false;
        try{
            this.setConexion((DriverManager.getConnection(this.url,this.usuario,this.contraseña)));
            exito = true;
        }catch (SQLException e){
            exito = false;
            System.err.println("Surgio un error al conectar" + e.getMessage());
            
        }
        return exito;
    }
    public void desconectar(){
        try{
            if(!this.conexion.isClosed()) this.getConexion().close();
        }catch(SQLException e){
            System.err.println("error No fue posible cerrar la conexion"+ e.getMessage());
            
        }
       
    }
}
