/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package mdd;

/**
 *
 * @author emoru
 */
import modelo.Productos;
import vista.dlgProducto;
import modelo.DbProducto;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import java.util.Date;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class controlador implements ActionListener{
    private Productos P;
    private dlgProducto vista;
    private DbProducto Pro;
    private boolean A = false;
    public controlador(Productos P,DbProducto Pro,dlgProducto vista){
        this.P = P;
        this.vista = vista;
        this.Pro = Pro;
        vista.btnSalir.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnBuscarAct.addActionListener(this);
        vista.btnActivar.addActionListener(this);
    }
    private void limpiar(){
            vista.txtFecha.setDate(null);
            vista.txtNombre.setText("");
            vista.txtIdProducto.setText("");
            vista.txtPrecio.setText("");
            vista.SpStatus.setValue(1);
            A = (false);
    }
    private void deshabilitar(){
            vista.txtFecha.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtIdProducto.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            
            vista.btnGuardar.setEnabled(false);
            vista.btnBuscar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnDeshabilitar.setEnabled(false);
    }
    private void habilitar(){
            vista.txtFecha.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtIdProducto.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.SpStatus.setEnabled(true);
            
            vista.btnGuardar.setEnabled(true);
            vista.btnBuscar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnDeshabilitar.setEnabled(true);
    }
    private void cargarDatos(){
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Productos> lista2 = new ArrayList<>();
        try{
            lista2 = Pro.lista2();
        }
        catch(Exception ex){
            
        }
        modelo.addColumn("Id");
        modelo.addColumn("Codigo");
        modelo.addColumn("Nombre");
        modelo.addColumn("Fecha");
        modelo.addColumn("Precio");
        modelo.addColumn("Status");
        for(Productos producto: lista2){
            modelo.addRow(new Object[]{producto.getIdProducto(), producto.getCodigo(), producto.getNombre(), producto.getFecha(), producto.getPrecio(), producto.getStatus()});
        }
        vista.tblTabla1.setModel(modelo);
    }
    
    private void cargarDatosDes(){
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Productos> lista2 = new ArrayList<>();
        try{
            lista2 = Pro.lista();
        }
        catch(Exception ex){
        }
        modelo.addColumn("Id");
        modelo.addColumn("Codigo");
        modelo.addColumn("Nombre");
        modelo.addColumn("Fecha");
        modelo.addColumn("Precio");
        modelo.addColumn("Status");
        for(Productos producto: lista2){
            modelo.addRow(new Object[]{producto.getIdProducto(), producto.getCodigo(), producto.getNombre(), producto.getFecha(), producto.getPrecio(), producto.getStatus()});
        }
        vista.tblTabla2.setModel(modelo);
    }
    private void iniciarVista(){
        cargarDatos();
        cargarDatosDes();
        vista.setTitle(":: Productos ::");
        vista.setSize(600,620);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==vista.btnNuevo){
            habilitar();
            cargarDatos();
            cargarDatosDes();
            limpiar();
        }
        
        if(e.getSource()==vista.btnGuardar){
            try {
                P.setCodigo(vista.txtIdProducto.getText());
                P.setNombre(vista.txtNombre.getText());
                P.setStatus((int) vista.SpStatus.getValue());
                P.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                P.setIdProducto(Integer.parseInt(vista.txtIdProducto.getText()));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String Fecha = sdf.format(vista.txtFecha.getDate());
                P.setFecha(Fecha);
                if (!Pro.isExiste(vista.txtIdProducto.getText())) {
                    A = true;
                    if (A) {
                        Pro.insertar(P);
                        JOptionPane.showMessageDialog(null, "Registro Guardado");
                        cargarDatos();
                        cargarDatosDes();
                    }
                } else {
                    Pro.actualizar(P);
                    JOptionPane.showMessageDialog(null, "Registro Actulizado");
                    cargarDatos();
                    cargarDatosDes();
                }
            } catch (SQLException d) {
                d.printStackTrace();
            } catch (Exception ex1) {
                System.err.println("Surgió un error" + ex1);
            }
        }
        
        if(e.getSource()==vista.btnBuscar){
            try{
                Productos res = new Productos();
                String codigo = vista.txtIdProducto.getText();
                if(Pro.isExiste(vista.txtIdProducto.getText())){
                    res = (Productos)Pro.buscar(codigo);
                    vista.txtIdProducto.setEnabled(false);
                    vista.txtNombre.setText(res.getNombre());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date fecha = sdf.parse(res.getFecha());
                    vista.txtFecha.setDate(fecha);
                    vista.SpStatus.setValue(res.getStatus());
                    vista.txtPrecio.setText(String.valueOf(res.getPrecio()));
                    this.A = true;
                    JOptionPane.showMessageDialog(vista, "Se localizo");
                    vista.SpStatus.setEnabled(false);
                }
                else{
                    JOptionPane.showMessageDialog(vista, "codigo no existe");
                }
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(vista, "codigo no existe");
                System.out.println("Surgió un error" + ex.getMessage());
                vista.btnLimpiar.doClick();
            }
        }
        
        if(e.getSource()==vista.btnBuscarAct){
            try{
                Productos res = new Productos();
                if(Pro.isExiste(vista.txtCodigoAct.getText()) == true){
                    res = (Productos)Pro.buscar(vista.txtCodigoAct.getText());
                    vista.txtNombreAc.setText(res.getNombre());
                    JOptionPane.showMessageDialog(vista, "ENCONTRADO");
                }
                else{
                    JOptionPane.showMessageDialog(vista, "CÓDIGO INEXISTENTE");
                }
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(vista, "CÓDIGO INEXISTENTE");
                System.out.println("Surgió un error" + ex.getMessage());
                vista.btnLimpiar.doClick();
            }
        }
        
        if(e.getSource()==vista.btnActivar){
            try{
                Pro.habilitar(P, vista.txtCodigoAct.getText());
                cargarDatos();
                cargarDatosDes();
            }
            catch(Exception ex){
                System.out.println("Surgió un error" + ex.getMessage());
            }
            
        }
        
        if(e.getSource()==vista.btnDeshabilitar){
            try{
                Pro.deshablitar(P, vista.txtIdProducto.getText());
                cargarDatos();
                cargarDatosDes();
            }
            catch(Exception ex){
                System.out.println("Surgió un error" + ex.getMessage());
            }
            
        }
        
        if(e.getSource()==vista.btnSalir){
            int option=JOptionPane.showConfirmDialog(vista,"Seguro que quieres salir",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
        if(e.getSource()==vista.btnCancelar){
            limpiar();
            deshabilitar();
        }
        
        if(e.getSource()==vista.btnLimpiar){
            limpiar();
        }
    }
    
    public static void main(String[] args) {
            Productos P = new Productos();
            DbProducto Pro = new DbProducto();
            dlgProducto vista = new dlgProducto(new JFrame(),true);
            controlador contra = new controlador (P,Pro,vista);
            contra.iniciarVista();
    }
}
